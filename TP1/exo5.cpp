#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){

    if(n>0)
    {
        if(z.length()>2)
        {
            return n;
        }
        else
        {
            float x=z.x;
            float y=z.y;
            float xpoint = point.x;
            float ypoint = point.y;
            Point f;
            f.x=(x*x -y*y + xpoint);
            f.y=(2*x*y+ypoint);
            return isMandelbrot(f,n-1,point);
        }
    }
    else{
        return 0;
    }


}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



