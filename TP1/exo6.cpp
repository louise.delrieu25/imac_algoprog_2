#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* next;
};

struct Liste{
    Noeud* first;
    int nbNoeud;
    Noeud* last;
};

struct DynaTableau{
    int* donnees;
    int taille;
    int capacite;
};


void initialise(Liste* liste)
{
    liste->first = NULL;
    liste->last = NULL;
    liste->nbNoeud = 0;
}

bool est_vide(const Liste* liste)
{
    if(liste->nbNoeud ==0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* nouveau = new Noeud;
    nouveau->donnee = valeur;
    nouveau->next = NULL;

    if(liste->first == NULL)
    {
        liste->first = nouveau;
        liste->last = nouveau;
    }
    else
    {
        liste->last->next = nouveau;
        liste->last = nouveau;
    }
    liste->nbNoeud = liste->nbNoeud + 1;
}

void affiche(const Liste* liste)
{
    int count = 1;
    Noeud* tmp = liste->first;

    while(tmp != NULL)
    {
        cout << "Valeur n°" << count << ":" << tmp->donnee << endl;
        tmp = tmp->next;
        count ++;
    }
}

int recupere(const Liste* liste, int n)
{
    if(n>=0 && n<=liste->nbNoeud)
    {
        Noeud* tmp = liste->first;
        for(int i =0;i<n-1; i++)
        {
            tmp = tmp->next;
        }
        return tmp->donnee;
    }
    else
    {
       return -1;
    }

}

int cherche(const Liste* liste, int valeur)
{
    int count =0;
    int index = 1;
    bool cherche = false;
    Noeud* tmp = liste->first;

    while(!cherche && tmp != NULL)
    {
        if(tmp->donnee == valeur)
        {
            index= count;
            cherche = true;
        }
        tmp = tmp->next;
        count ++;
    }
    return index;
}

void stocke(Liste* liste, int n, int valeur)
{
    if(n>=0 && n<=liste->nbNoeud)
    {
        Noeud* tmp = liste->first;
        for(int i = 0;i<n-1; i++)
        {
            tmp = tmp->next;
        }
        tmp->donnee = valeur;
    }
    else
    {
        cout << " ERROR"<< endl;
    }

}

void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->taille > tableau->capacite)
    {
        tableau->capacite ++;
        int *plusdonnees= (int*)realloc(tableau->donnees,tableau->capacite*sizeof(int));
        if (plusdonnees != NULL)
        {
            tableau->donnees = plusdonnees;
            tableau->donnees[tableau->taille]=valeur;
            tableau->taille ++;

        }
        else
        {
            exit(1);
        }
    }
    else
    {
        tableau->donnees[tableau->taille]=valeur;
        tableau->taille ++;

    }
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->donnees =(int*)malloc(capacite * sizeof (int));
    tableau->capacite=capacite;
    tableau->taille=0;
    if(tableau == NULL)
    {
        exit(1);
    }
}

bool est_vide(const DynaTableau* tableau)
{
    if(tableau->taille == 0)
    {
        return true;
    }
    else
    {
        return false;
    }

}

void affiche(const DynaTableau* tableau)
{
    for(int i=0; i<tableau->taille; i++)
    {
        cout << "Valeur n°"<< i <<" : "<< tableau->donnees[i] << endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    if(n>=0 && n< tableau->taille)
    {
        return tableau->donnees[n];
    }
    else
    {
        cout<<"ERROR"<<endl;
        return -1;
    }

}

int cherche(const DynaTableau* tableau, int valeur)
{
    int count = 0;
    int index = -1;
    bool cherche = false;

    while(!cherche && count < tableau->taille)
    {
        if(tableau->donnees[count] == valeur)
        {
            index = count;
            cherche = true;
        }
        count ++;
    }
    return index;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if(n>=0 && n<tableau->taille)
    {
        tableau->donnees[n] = valeur;
    }
    else
    {
        cout<<"ERROR"<<endl;
    }
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste,valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    Noeud* tmp = liste->first;
    int valeur = tmp->donnee;
    liste->first = liste->first->next;
    liste->nbNoeud = liste->nbNoeud-1;
    free(tmp);
    return valeur;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud* nouveau = new Noeud;
    nouveau->donnee = valeur;
    nouveau->next = liste->first;
    liste->first = nouveau;
    liste->nbNoeud = liste->nbNoeud +1;
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    Noeud* tmp = liste->first;
    int valeur = tmp->donnee;
    liste->first = liste->first->next;
    liste->nbNoeud = liste->nbNoeud -1;
    free(tmp);
    return valeur;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&liste) && compteur > 0)
    {
        std::cout << retire_file(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_file(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
