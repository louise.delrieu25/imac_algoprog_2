#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;


void binarySearchAll(Array& array, int toSearch, int& indexMin, int& indexMax)
{
    indexMin = indexMax = -1;

    int start = 0;
    int end = array.size();
    int foundIndex = -1;
    //int count = 1;
    while (start<end)
    {
        int mid = (start+end)/2;
        if (toSearch > array[mid])
        {
            start = mid + 1;
        }
        else if (toSearch < array[mid])
        {
            end = mid;
        }
        else
        {
            foundIndex= mid;
            end = mid;
        }
        indexMin = foundIndex;
    }

    start = 0;
    end = array.size();

    while (start<end)
    {
        int mid = (start+end)/2;
        if (toSearch > array[mid])
        {
            start = mid + 1;
        }
        else if (toSearch < array[mid])
        {
            end = mid;
        }
        else
        {
            foundIndex= mid;
            end = mid;
        }
        indexMax = foundIndex;
    }
    /*if(foundIndex != -1)
    {
        indexMin = foundIndex;
        indexMax = foundIndex;
        while(array[foundIndex-count]==toSearch || array[foundIndex + count]==toSearch)
        {
            if (array[foundIndex-count]==toSearch)
            {
                indexMin = foundIndex-count;
            }
            if(array[foundIndex + count]==toSearch)
            {
                indexMax = foundIndex + count;
            }
            count ++;
        }
    }*/


}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchAllWindow(binarySearchAll);
	w->show();

	return a.exec();
}
