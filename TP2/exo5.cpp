#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    if(origin.size()<=1){
            return;
        }


	// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split
    int i, j=0;
        for(i=0; i<origin.size()/2;i++){
            first[i]=origin[i];
        }
        for(i;i<origin.size();i++){
            second[j]=origin[i];
            j++;
        }


	// recursiv splitAndMerge of lowerArray and greaterArray
    splitAndMerge(first);
    splitAndMerge(second);


	// merge
    merge(first,second,origin);
}

void merge(Array& first, Array& second, Array& result)
{

    int index_first=0;
        int index_second=0;
        int result_size=first.size()+second.size();

        for (int i=0; i<result_size;i++){

            if(index_first==first.size()){
                result[i]=second[index_second];
                index_second++;
            }

            else if(index_second==second.size()){
                result[i]=first[index_first];
                index_first++;
            }

            else if (first[index_first]<=second[index_second]){
                result[i]=first[index_first];
                index_first++;
            }

            else{
                result[i]=second[index_second];
                index_second++;
            }
        }


}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
