#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{

    return nodeIndex*2 +1;
}

int Heap::rightChild(int nodeIndex)
{

    return nodeIndex*2+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
    (*this)[i]= value;
    while (i>0 && (*this)[i]> (*this)[(i-1)/2])
    {
        swap(i,(i-1)/2);
        i= (i-1)/2;

    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
    int i_max = nodeIndex;
    if(rightChild(nodeIndex)<heapSize && (*this)[rightChild(nodeIndex)]>(*this)[i_max])
    {
        i_max=rightChild(nodeIndex);
    }
    if(leftChild(nodeIndex)<heapSize && (*this)[leftChild(nodeIndex)]>(*this)[i_max])
    {
        i_max= leftChild(nodeIndex);
    }

    if (i_max!=nodeIndex)
    {
        swap(nodeIndex,i_max);
        heapify(heapSize,i_max);
    }


}

void Heap::buildHeap(Array& numbers)
{
    int n = numbers.size();
    int i =0;
    while (i>=n)
    {
        (*this)[i]= numbers[i];
        (*this)[rightChild(i)]= numbers[rightChild(i)];
        (*this)[leftChild(i)]=numbers[leftChild(i)];
        i=rightChild(i);
    }
    heapify(n,0);
}

void Heap::heapSort()
{
    for(int i= (*this).size()-1;i>=0;i --)
    {
        swap(0,i);
        heapify(i,0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
